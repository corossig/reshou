#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys, os
from collections import deque
from PyQt4 import QtGui, QtCore, uic
import cv2

src_dir = os.path.dirname(os.path.abspath(__file__))

class CLQPixpmal(QtGui.QPixmap) :
    def __init__(self, iplimage) :
        height = len(iplimage)
        width = len(iplimage[0])
        QtGui.QPixmap.__init__(self, width, height)
        self.convertFromImage(QtGui.QImage(iplimage.data, width, height, QtGui.QImage.Format_RGB888).rgbSwapped())
 
class Area(QtGui.QTreeWidgetItem):
    def __init__(self, index, center):
        QtGui.QTreeWidgetItem.__init__(self, [str(index)])
        self.index = index
        self.center = center
        self.pixels = [center]
        self.distance = 50
        self.improve = 150

class TestApp(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        
        self.ui = uic.loadUi(os.path.join(src_dir, "ui", "main.ui"))
        self.ui.show()

        self.scene = QtGui.QGraphicsScene(self);
        self.ui.graphicsView.setScene(self.scene);
        self.ui.graphicsView.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.ui.graphicsView.setResizeAnchor(QtGui.QGraphicsView.AnchorViewCenter)
        self.ui.graphicsView.wheelEvent = self.wheelEvent
        self.graphic = None
        
        self.connect_all()
        
        self.openNewImage(os.path.join(src_dir, "..", "data", "Jeux1", "ORIGINAL_IMAGES", "image0_qgpAzkGE.jpg"))

    def wheelEvent(self, event):
        if event.delta() > 0:
            self.ui.graphicsView.scale(2, 2)
        elif event.delta() < 0:
            self.ui.graphicsView.scale(0.5, 0.5)
        
        
    def connect_all(self):
        self.connect(self.ui.actionQuit, QtCore.SIGNAL("triggered()"), QtGui.qApp.quit)
        self.connect(self.ui.actionOpenImage, QtCore.SIGNAL("triggered()"), self.onOpenImage)
        self.connect(self.ui.treeWidget, QtCore.SIGNAL("itemClicked(QTreeWidgetItem,int)"), self.selectArea)
        self.connect(self.ui.sbImprove, QtCore.SIGNAL("valueChanged(int)"), self.onImproveChanged)
        self.connect(self.ui.sbDistance, QtCore.SIGNAL("valueChanged(int)"), self.onDistanceChanged)
        self.scene.mousePressEvent = self.onMouse
        
    def onOpenImage(self):
        img_file=QtGui.QFileDialog.getOpenFileName(self, "Open image file", "",
                                                   "Images (*.jpg *.jpeg *.png);; All files (*.*)")
        self.openNewImage(img_file)

    def openNewImage(self, img_file):
        self.img = cv2.imread(str(img_file))
        self.hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)
        self.original_hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)

        if self.graphic != None:
            self.scene.removeItem(self.graphic)

        self.height = len(self.img)
        self.width = len(self.img[0])
        print(self.width)
        pixmap = CLQPixpmal(self.img)
        self.graphic = self.scene.addPixmap(pixmap);
        self.scene.setSceneRect(QtCore.QRectF(pixmap.rect()));
        self.nb_pixel = self.width* self.height
        
        self.pixel_map = [-1] * self.nb_pixel
        self.counter = 0
        self.ui.treeWidget.clear()
        self.areas = []
        

    def onMouse(self, event):
        pos = event.scenePos()
        pixel = int(pos.x()) + int(pos.y())*int(self.width)
        print(int(pos.x()), int(pos.y()))
        print(self.pixel_map[pixel])
        if self.pixel_map[pixel] != -1 :
            self.selectArea(self.areas[self.pixel_map[pixel]], 0)
            self.ui.treeWidget.setCurrentItem(self.areas[self.pixel_map[pixel]])
        else :
            area = Area(len(self.areas), pixel)
            self.areas.append(area)
            self.ui.treeWidget.addTopLevelItem(area)
            self.ui.treeWidget.setCurrentItem(area)
            self.selectArea(area, 0)
            self.defineArea(area)
            self.improvePixels(area)

    def selectArea(self, area, index) :
        self.currentArea = area
        self.ui.labelCenter.setText("%d, %d" % (area.center%self.width, area.center/self.width))
        self.ui.sbDistance.setValue(area.distance)
        self.ui.sbImprove.setValue(area.improve-100)

    def onImproveChanged(self, value) :
        if self.currentArea != None :
            self.currentArea.improve = 100+value
            self.improvePixels(self.currentArea)
    
    def onDistanceChanged(self, value) :
        if self.currentArea != None :
            self.currentArea.distance = value
            for pixel in self.currentArea.pixels :
                x = int(pixel%self.width)
                y = int(pixel/self.width)
                self.hsv[y][x][2] = self.original_hsv[y][x][2]
                self.pixel_map[pixel] = -1
            self.pixel_map[self.currentArea.center] = self.currentArea.index
            self.graphic.setPixmap(CLQPixpmal(cv2.cvtColor(self.hsv, cv2.COLOR_HSV2BGR)))
            self.currentArea.pixels = [self.currentArea.center]
            self.defineArea(self.currentArea)
            self.improvePixels(self.currentArea)
        
    def defineArea(self, area):
        print("Begin area %d" % (area.center,))
        pixels = deque([area.center])
        
        def testPixel(x, y, n_x, n_y):
            n_pixel = int(n_x + n_y * self.width)
            if self.pixel_map[n_pixel] == -1:
                diff = int(self.hsv[n_y][n_x][2]) - int(self.hsv[y][x][2])
                diff *= diff
                if diff < area.distance :
                    self.pixel_map[n_pixel] = area.index
                    area.pixels.append(n_pixel)
                    pixels.append(n_pixel)
        
        self.pixel_map[area.center] = area.index
        while len(pixels) > 0 :
            pixel = int(pixels.popleft())
            x = int(pixel%self.width)
            y = int(pixel/self.width)
            
            if x > 0:
                testPixel(x, y, x-1, y)
            
            if x < self.width-1:
                testPixel(x, y, x+1, y)
                
            if y > 0:
                testPixel(x, y, x, y-1)
            
            if y < self.height-1:
                testPixel(x, y, x, y+1)
            # At the end do dilatation/erosion
        area.pixels.sort()
        print("End area %d" % (area.center,))
            
        
    def improvePixels(self, area) :
        print("Begin apply %d" % (len(area.pixels),))
        for pixel in area.pixels :
            x = int(pixel%self.width)
            y = int(pixel/self.width)
            self.hsv[y][x][2] = int(float(area.improve)/100*self.original_hsv[y][x][2])
        print("End apply %d" % (len(area.pixels),))
        self.graphic.setPixmap(CLQPixpmal(cv2.cvtColor(self.hsv, cv2.COLOR_HSV2BGR)))
            
        
        
    
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    win = TestApp()
    sys.exit(app.exec_())
