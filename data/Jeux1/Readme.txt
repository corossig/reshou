Ce dossier contient les fichiers suivants:

/ORIGINAL_IMAGES :
Dossier contenant les images originales ayant servi � la reconstruction 3D.
Ces images sont donn�es � titre purement indicatifs, elles ne sont pas n�cessaires pour le concours, sauf si les �tudiants souhaitent reprendre les algorithmes de reconstruction 3D.
Les images sont en format jpg.

/RECONSTRUCTIONS
Dossier contenant les r�sultats de reconstruction

	/ORIENTEDIMAGES
	Images corrig�es (ortho rectification) au format jpg
	Les matrices de transformation et position des camera (fichier .cam) au format ascii		
		fichier .cam (format ascii)
			P
			P00 P01 P02 P03
			P10 P11 P12 P13
			P20 P21 P22 P23
			K
			K00 K01 K02
			K10 K11 K12
			K20 K21 K22
			R
			R00 R01 R02
			R10 R11 R12
			R20 R21 R22
			T
			T0 T1 T2
			Distortion
			k1 k2
			old index
			OI
			index (facultatif)
			I

			La matrice P de taille 3x4 est la matrice de projection telle qu'un point 3D [X Y Z]' est projet� en un point 2D de l'image rectifi�e [x y]' selon [x y 1]' ~ P * [X Y Z 1]'
			P est � un facteur multiplicatif pr�s, donc en pratique, on calcule [x y w]' = P * [X Y Z 1]'
			et on effectue une normalisation homog�ne du vecteur r�sultat : [x/w y/w 1]'
			K, R et T sont les param�tres internes et externes de la cam�ra tels que P = K[R T].
			K est la matrice classique des param�tres internes contenant la distance focale et le point principal.
			La translation et la rotation de la cam�ra sont donn�es par T et R. Plus exactement, la position C de la cam�ra est donn�e par C = -R'T.
			k1 et k2 sont les param�tres classiques de distorsion radiale d'ordres 1 et 2. 
			old index est l'index original de l'image. 
			Il n'y a pas de distorsion tangentielle car elle est n�gligeable sur les appareils photos r�cents.
			Certaines images peuvent �tre �limin�es lors du recalage des images entre elles si par exemple elles ne pr�sentent pas suffisamment de recouvrement avec les autres. Elles sont alors renum�rot�es.
			index permet de faire le lien entre l'image renum�rot�e et son ancien index dans la s�quence.


	/POINTCLOUDS
	nuage de points 3D reconstruits
		fichier .pts (format ascii) :
			nb points
			x	y	z 	// position du point
			nx	ny	nz	// normale au point	
			r	v	b	// couleur du point
			nbI			// nombre d'images sur lesquelles le point est visible
			I1 I2 ...		// liste des images
			x	y	z
			...
	
		fichier .wrl : nuage de point au format VRML

	/TRIMESH
	Dossier contenant les surfaces triangul�es 3D reconstruites
		.mesh (format binaire)
			int : nombre de points
			int : nombre de triangle
			int : nombre index texture (non utilis� ici)
			int : nombre de coordonn�es de texture (non utilis� ici)
			double(s) : liste des coordonn�es x,y,z des points -> 3 x nombre de points
			int(s) : liste des triangles i1, i2, i3 (indices de points) -> 3 x nombre de triangles

		.wrl (format VRML) : 2 fichiers fournis (avec ou sans texture)

		/trimeshCollada : format Collada
		